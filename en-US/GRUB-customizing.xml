<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Fedora_Multiboot_Guide.ent">
%BOOK_ENTITIES;

]>
<section id="GRUB-configuration">
  <title>Customizing GRUB entries</title>
  <para>
    Templates used to create the GRUB configuration file are found in two places. General settings for GRUB itself are set in <filename>/etc/default/grub</filename>, and settings for operating systems are found within <filename>/etc/grub.conf.d</filename>. After making changes to these files, apply them using the instuctions in <xref linkend="GRUB-mkconfig" />
  </para>
  <section>
    <title>GRUB Global settings</title>
    <para>
      This section covers existing and possible GRUB settings for <filename>/etc/default/grub</filename>.  For a complete overview, see <ulink url="http://www.gnu.org/software/grub/manual/grub.html#Configuration" />
    </para>
    <table>
      <tgroup cols="3" align="left" colsep="1" rowsep="1">
        <colspec colname="option" />
        <colspec colname="function" />
        <colspec colname="example" />
        <thead>
          <row>
            <entry>GRUB Option</entry>
            <entry>Function</entry>
            <entry>Example Usage</entry>
          </row>
        </thead>
        <tbody>
          <row>
            <entry>GRUB_TIMEOUT</entry>
            <entry>
              Number of seconds to display menu before loading default entry. 0 boots immediately, -1 waits indefinitely.
            </entry>
            <entry>
              GRUB_TIMEOUT=0
            </entry>
          </row>
          <row>
            <entry>GRUB_HIDDEN_TIMEOUT</entry>
            <entry>Number of seconds to wait for a keypress before displaying the menu. If no key is pressed, boots the default entry. Often used with GRUB_TIMEOUT=0.
            </entry>
            <entry>
              GRUB_HIDDEN_TIMEOUT=0
            </entry>
          </row>
          <row>
            <entry>
              GRUB_HIDDEN_TIMEOUT_QUIET
            </entry>
            <entry>
              If set, does not display countdown during GRUB_HIDDEN_TIMOUT wait.
            </entry>
            <entry>
              GRUB_HIDDEN_TIMEOUT_QUIET=true
            </entry>
          </row>
          <row>
            <entry>GRUB_CMDLINE_LINUX</entry>
            <entry>
              Arguments that are added to menu entries for Linux systems.
            </entry>
            <entry>
              GRUB_CMDLINE_LINUX="rhgb quiet acpi_backlight=vendor"
            </entry>
          </row>
        </tbody>
      </tgroup>
    </table>
    <para>This second set of GRUB options can be used to change the appearance of GRUB. By default, GRUB presentation is minimal, allowing the GRUB to load quickly and the system to boot as quickly as possible.
    </para>
    <table>
      <title>Aesthetic GRUB options</title>
      <tgroup cols="3" align="left" colsep="1" rowsep="1">
        <colspec colname="option" />
        <colspec colname="function" />
        <colspec colname="example" />
        <thead>
          <row>
            <entry>GRUB Option</entry>
            <entry>Function</entry>
            <entry>Example Usage</entry>
          </row>
        </thead>
        <tbody>
          <row>
            <entry>GRUB_TERMINAL</entry>
            <entry>
              Sets the value for both GRUB_TERMINAL_INPUT and GRUB_TERMINAL_OUTPUT. Use gfxterm for graphical output, or serial to output through a serial port.
            </entry>
            <entry>GRUB_TERMINAL=gfxterm</entry>
          </row>
          <row>
            <entry>GRUB_BACKGROUND</entry>
            <entry>Set a background image for use with gfxterm. The file should be in the boot partition and of PNG, TGA, or JPG format.
            </entry>
            <entry>
              GRUB_BACKGROUND=hot-dog.png
            </entry>
          </row>
          <row>
            <entry>GRUB_THEME</entry>
            <entry>Points to the text file that defines a GRUB2 theme. None are included by default, due to the additional boot time required to load the theme. You can install the grub2-starfield-theme package for an example, or look on a website such as <ulink url="http://gnome-look.org" />
            </entry>
            <entry>
              GRUB_THEME=/usr/share/grub/themes/starfield/theme.txt
            </entry>
          </row>
        </tbody>
      </tgroup>
    </table>

  </section> 
</section>
