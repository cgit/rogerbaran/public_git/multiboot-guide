﻿Dual booting 
With 
Fedora and multiple operating systems




Table of Contents


Introduction


1.1 About Fedora
1.2 About Windows 8
1.3 Getting further help


Goals


New Windows 8 boot sequence


3.1 What is UEFI
3.2 New Windows 8 boot sequence


Installing Windows 8 and Fedora Together.


4.1 Fresh install of Windows 8 with Fedora not installed.
4.2 Installing Fedora with Windows 8 already Installed.
4.3 Running Windows 8, Windows 7, and Fedora together.


A. Contributors


B. Revision History










Introduction


This guide covers dual booting of Fedora, a Linux distribution built on free and open source software, with Microsoft Windows 8. This manual will help you install Windows 8 and Fedora together on a single machine. Covered in this manual will also be different type of configurations as well as covering the Microsoft Windows 8 Bootloader in detail and the options it provides, for a clean install and boot. This guide will not focus on explaining what Fedora or Windows 8 is, but provide links to their prospective sites.
About Windows 8
To find out more about Microsoft Windows 8, refer to http://windows.microsoft.com/en-us/windows-8/meet
About Fedora
To find out more about Fedora, refer to http://fedoraproject.org/. To read other documentation on Fedora related topics, refer to http://docs.fedoraproject.org/.
Getting further help
For information on additional help resources for Fedora, visit http://fedoraproject.org/wiki/Communicate.
Audience
This guide is intended for Fedora and Microsoft users of all levels of experience, however it treats the installation process and the Windows 8 boot loader’s many options in far greater detail. With that said the user should have a moderate knowledge of Microsoft Windows.


Goals
This guide helps a reader:
1. How to Install Windows 8 and Fedora together so that grub will boot into each OS
2. Understand the Windows 8 boot sequence and how it differs from Windows 7
3. How to run Windows 7, Windows 8 and Fedora together.
4. Answer some common questions about the Windows 8 boot loader options and how they relate to dual booting.
5. Provide some answers to some frequently asked questions that I have heard or may come up.




Overview of boot sequences


3.1 What is UEFI


UFEI stands for Unified Extensible Firmware Interface, UEFI is meant to replace the Basic Input/Output System (BIOS) firmware interface. UEFI defines an interface between personal-computer operating systems and platform specific firmware. Diving deeper into the interface, its inner workings consist of data that contain platform specific information, boot, and runtime services that are available to the operating system and its bootloader.




3.2 New Windows 8 boot sequence


Microsoft with the release of Windows 8 has chosen to change the bootloader and the manner in which operating systems are loaded once selected from the bootloader menu. The typical process that most bootloaders go through is something like this: The power button on the computer is pressed and the device turns on and starts the BIOS power on self test. The MBR on the hard drive is referenced accessing the bootloader and the user is given a selection menu to choose which operating system to use. Below is a screenshot showing the process of how the boot process works:
















With Microsoft Windows 8, the process has changed completely, and through testing this is now along the lines of what takes place:



















As you notice from the picture above, the change is very small but is subtle enough to not show the boot menu the second time around, the computer does a reboot after making the selection. Performing various installs and going through testing it seems that the new bootloader and menu interface is possibly running in a protected mode. The bootloader needs to reboot the system back into a real time mode that the Windows 8 kernel can initialize from.




Installing Windows 8 and Fedora Together.




4.1 Fresh install of Windows 8 with Fedora not installed.


The goal of this guide is to provide support for multibooting different operating systems with Fedora. That being the case the guide will not go into details of how to perform a clean install of Windows 8, but provide a link to a detailed guide on the subject. 
A guide showing how to perform a clean install of Microsoft Windows 8 in 31 detailed steps can be located here: http://pcsupport.about.com/od/windows-8/ss/windows-8-clean-install-part-1.htm


4.2 Installing Fedora with Windows 8 already Installed.
